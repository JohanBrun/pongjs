var canvas;
var game;
var anim;
var initialYPos = canvas.height /2 - PLAYER_HEIGHT /2

const PLAYER_HEIGHT = 100;
const PLAYER_WIDTH = 5;

function Draw() {
    var context = canvas.getContext("2d");

    //draw field
    context.fillStyle = 'black';
    context.fillRect(0, 0, canvas.width, canvas.height);

    //draw middle line
    context.strokeStyle = 'white';
    context.beginPath();
    context.moveTo(canvas.width / 2, 0);
    context.lineTo(canvas.width / 2, canvas.height);
    context.stroke();

    //draw players
    context.fillStyle = "white";
    context.fillRect(0, game.player.y, PLAYER_WIDTH, PLAYER_HEIGHT);
    context.fillRect(canvas.width - PLAYER_WIDTH, game.computer.y, PLAYER_WIDTH, PLAYER_HEIGHT);

    //draw ball
    context.beginPath();
    context.fillStyle = "white";
    context.arc(game.ball.x, game.ball.y, game.ball.r, 0, Math.PI *2, false);
    context.fill();
}

function ComputerMove() {
    game.computer.y += game.ball.speed.y *0.77
}

function BallMove() {

    //Rebounds on top and bottom
    if (game.ball.y > canvas.height || game.ball.y < 0) {
        game.ball.speed.y *= -1;
    }

    if (game.ball.x > canvas.width - PLAYER_WIDTH) {
        Collide(game.computer);
    } else if (game.ball.x < PLAYER_WIDTH) {
        Collide(game.player)
    }

    game.ball.x += game.ball.speed.x;
    game.ball.y += game.ball.speed.y;
}

function ChangeDirection(playerPosition) {
    var impact = game.ball.y - playerPosition - PLAYER_HEIGHT /2;
    var ratio = 100 / (PLAYER_HEIGHT /2);

    game.ball.speed.y = Math.round(impact * ratio / 10);
}

function Collide(player) {
    if (game.ball.y < player.y || game.ball.y > player.y + PLAYER_HEIGHT) {
        ReinitGame()

        //update score
        if (player == game.player) {
            game.computer.score++;
            document.querySelector('#computer-score').textContent = game.computer.score;
        }
        else {
            game.player.score ++;
            document.querySelector('#player-score').textContent = game.player.score;
        }
    }
    else {
        game.ball.speed.x *= -1.2;
        ChangeDirection(player.y)
    }
}

function Play() {
    Draw();
    ComputerMove();
    BallMove();
    anim = requestAnimationFrame(Play);
}

function Stop() {
    cancelAnimationFrame(anim);
    ReinitGame()

    game.computer.score = 0;
    game.player.score = 0;

    document.querySelector('#computer-score').textContent = game.computer.score;
    document.querySelector('#player-score').textContent = game.player.score;

    Draw();
}

function PlayerMove(event) {
    
    //get the mouse location in the canvas
    var canvasLocation = canvas.getBoundingClientRect();
    var mouseLocation = event.clientY - canvasLocation.y;

    if (mouseLocation < PLAYER_HEIGHT /2) {
        game.player.y = 0;
    } 
    else if (mouseLocation > canvas.height - PLAYER_HEIGHT / 2) {
        game.player.y = canvas.height - PLAYER_HEIGHT;
    }
    else {
        game.player.y = mouseLocation - PLAYER_HEIGHT / 2;
    }
}

function ReinitGame() {
    game.ball.x = canvas.width / 2;
    game.ball.y = canvas.height / 2;
    game.player.y = canvas.height / 2 - PLAYER_HEIGHT / 2;
    game.computer.y = canvas.height / 2 - PLAYER_HEIGHT / 2;
    game.ball.speed.x = 2;
}

document.addEventListener("DOMContentLoaded", function () {
    canvas = document.getElementById('canvas');
    game = {
        player: {
            y: initialYPos,
            score: 0,
        },
        computer: {
            y: initialYPos,
            score: 0,
        },
        ball: {
            x: canvas.width / 2,
            y: canvas.height / 2,
            r: 5,
            speed: {
                x: 2,
                y: 2
            }
        }
    }

    canvas.addEventListener('mousemove', PlayerMove);

    Draw();
    document.querySelector('#start-game').addEventListener('click', Play);
    document.querySelector('#stop-game').addEventListener('click', Stop);
});